from collections import Counter, defaultdict
from random import shuffle
import os


def split_by_label(samples, labels):
    samples_by_label = defaultdict(list)
    for sample, label in zip(samples, labels):
        samples_by_label[label].append(sample)

    return samples_by_label


def split_by_ratio(samples, ratio):
    tmp = samples.copy()
    shuffle(tmp)
    first_sample_size = int(len(samples) * ratio)

    return (tmp[:first_sample_size], tmp[first_sample_size:])


def word_freq(messages_list):
    word_freq = Counter()
    for message in messages_list:
        words = message.split()
        word_freq.update(words)

    return word_freq


def total_unique_words(*words_lists):
    words_set = []
    for words_list in words_lists:
        words_set.extend(words_list)

    words_set = set(words_set)
    return len(words_set)


def accuracy(expected_labels, predicted_labels):
    correct = sum([1 if expected_label == predicted_label else 0
                   for expected_label, predicted_label in zip(expected_labels, predicted_labels)])
    return correct / len(expected_labels)


def confusion_matrix(expected_labels, predicted_labels, label):
    num_labels = len(expected_labels)
    true_positive = 0
    false_positive = 0
    true_negative = 0
    false_negative = 0

    for expected_label, predicted_label in zip(expected_labels, predicted_labels):
        if expected_label == label and predicted_label == label:
            true_positive += 1
        elif expected_label != label and predicted_label != label:
            true_negative += 1
        elif expected_label == label and predicted_label != label:
            false_negative += 1
        else:
            false_positive += 1

    return (true_positive/num_labels, false_negative/num_labels, false_positive/num_labels, true_negative/num_labels)


def read_files_from_folder(folder_path):
    files_path = sorted([os.path.join(folder_path, file_name)
                         for file_name in os.listdir(folder_path)])

    result = []
    for file_path in files_path:
        with open(file_path, 'r', encoding='latin') as fi:
            result.append(fi.read())

    return result


def read_files_from_many_folders(folders_list):
    result = []
    for folder in folders_list:
        result.extend(read_files_from_folder(folder))

    return result


def load_dataset(data_preprocessed=False):
    if data_preprocessed:
        # load processed data
        with open('./processed_email/SpamAssassin/easy_ham.txt', 'r', encoding='utf-8') as fi:
            easy_ham = fi.readlines()
        with open('./processed_email/SpamAssassin/hard_ham.txt', 'r', encoding='utf-8') as fi:
            hard_ham = fi.readlines()
        with open('./processed_email/SpamAssassin/spam.txt', 'r', encoding='utf-8') as fi:
            spam = fi.readlines()
    else:
        # load not processed data
        easy_ham_folders = [
            './dataset/SpamAssassin/easy_ham/20021010_easy_ham',
            './dataset/SpamAssassin/easy_ham/20030228_easy_ham',
            './dataset/SpamAssassin/easy_ham/20030228_easy_ham_2',
        ]
        hard_ham_folders = [
            './dataset/SpamAssassin/hard_ham/20021010_hard_ham',
            './dataset/SpamAssassin/hard_ham/20030228_hard_ham',
        ]
        spam_folders = [
            './dataset/SpamAssassin/spam/20021010_spam',
            './dataset/SpamAssassin/spam/20030228_spam',
            './dataset/SpamAssassin/spam/20030228_spam_2',
            './dataset/SpamAssassin/spam/20050311_spam_2',
        ]
        easy_ham = read_files_from_many_folders(easy_ham_folders)
        hard_ham = read_files_from_many_folders(hard_ham_folders)
        spam = read_files_from_many_folders(spam_folders)

    return (easy_ham, hard_ham, spam)


def average(array):
    return sum(array) / len(array)
